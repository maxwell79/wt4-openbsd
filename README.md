# wt-4-openbsd-port
OpenBSD Port of [Wt (Witty) 4.0.2](https://www.webtoolkit.eu/wt) for [OpenBSD](https://www.webtoolkit.eu/wt) 6.3


Wt is a web GUI library in modern C++. Quickly develop highly interactive web UIs with widgets, without having to write a single line of JavaScript. Wt handles all request handling and page rendering for you, so you can focus on functionality.

## Installation

Make sure OpenBSD 6.3 ports are installed at /usr/ports

cd /usr/ports

mkdir -p /usr/ports/mystuff/www

cd /usr/ports/mystuff/www

git clone https://github.com/djireland/wt-4-openbsd-port.git

cd wt-4-openbsd-port

make && make install


