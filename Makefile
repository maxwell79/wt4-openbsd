# $OpenBSD$

COMMENT=	C++ library and application server for web applications
GH_ACCOUNT=     emweb
GH_PROJECT=	wt
GH_COMMIT=	5457f9574821341c66bb4bbc9c77733cd8cbb653
DISTNAME=	wt-4.0.3
CATEGORIES=	www
HOMEPAGE=	http://www.webtoolkit.eu/
MAINTAINER=	David Ireland <djireland79@gmail.com>

# GPLv2+
PERMIT_PACKAGE_CDROM=	Yes

MODULES=	devel/cmake
RUN_DEPENDS= graphics/GraphicsMagick \
		devel/boost>=1.50 \
		www/fcgi \
		print/libharu \
		databases/mariadb,-main \
		databases/sqlite3 \
		databases/postgresql,-main
BUILD_DEPENDS=	${RUN_DEPENDS} \
		devel/doxygen
LIB_DEPENDS= 	www/fcgi>=2.4.0

CONFIGURE_STYLE=cmake
CONFIGURE_ENV=	HOME=${WRKDIR}
PORTHOME=	${WRKDIR}

MODCMAKE_WANTCOLOR=Yes

SHARED_LIBS +=  wtdbo                     4.0 # 4.0
SHARED_LIBS +=  wttest                    4.0 # 4.0
SHARED_LIBS +=  wt                        4.0 # 4.0
SHARED_LIBS +=  wtdbopostgres             4.0 # 4.0
SHARED_LIBS +=  wtdbosqlite3              4.0 # 4.0
SHARED_LIBS +=  wtfcgi                    4.0 # 4.0
SHARED_LIBS +=  wthttp                    4.0 # 4.0
SHARED_LIBS +=	wtdbomysql                4.0 # 4.0

.include <bsd.port.mk>
